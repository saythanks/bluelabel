<?php

return

    /*
    |--------------------------------------------------------------------------
    | BlueLabel API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [

        'hostname' => env('BLUE_LABEL_HOSTNAME', ''),
        'port' => env('BLUE_LABEL_PORT', '7890'),
        'timeout' => env('BLUE_LABEL_TIMEOUT', '2'),
        'aeon' => [
            'device_id' => env('BLUE_LABEL_DEVICE_ID', ''),
            'user_pin' => env('BLUE_LABEL_USER_PIN', ''),
            'device_serial' => env('BLUE_LABEL_DEVICE_SERIAL', ''),
        ],

    ];