<?php

namespace SayHi\BlueLabel;

use Illuminate\Support\ServiceProvider;

class BlueLabelServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/bluelabel.php', 'bluelabel');
    }

    public function boot()
    {

        $this->publishes(
            [
                __DIR__ . '/config/bluelabel.php' => config_path('bluelabel.php'),
            ]);
    }


}