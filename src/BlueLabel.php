<?php

namespace SayHi\BlueLabel;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use SayHi\BlueLabel\Exceptions\SocketException;
use SayHi\BlueLabel\Exceptions\ValidationException;
use SimpleXMLElement;
use Spatie\ArrayToXml\ArrayToXml;

class BlueLabel
{
    public $hostname;
    public $port;
    public $socket;
    public $timeout;

    public const ERROR_CODE_ACCOUNT_TYPE_NOT_VALID = 1;
    public const ERROR_CODE_MSISDN_VALIDATION_FAILED = 1800;
    public const ERROR_CODE_SUBSCRIBER_BARRED = 1811;


    public function __construct()
    {
        $this->hostname = config('bluelabel.hostname');
        $this->port = config('bluelabel.port');
        $this->timeout = (int) config('bluelabel.timeout');
        Log::withContext(
            [
                'host' => $this->hostname,
                'port' => $this->port,
            ]
        );
    }


    public function getSessionId(?string $network = null)
    {
        return $this->authenticate($network);
    }

    protected function authenticate(?string $network = null)
    {
        $arrayRequest = $this->getAuthenticationRequestData($network);
        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');
        Log::info('BlueLabel-Api: authenticate Request...');
        $authResponse = $this->sendXmlRequest($xmlRequest);
        $this->sessionId = (string) $authResponse->SessionId;

        return $this->sessionId;
    }

    public function airtimeTopup(int $amount, string $phoneNumber, string $network, string $reference = null )
    {
        if(!$reference){
            $reference = Str::random();
        }

        $arrayRequest = [
            'SessionId' => $this->authenticate($network),
            'EventType' => 'GetTopup',
            'event' => [
                'Reference' => $reference,
                'PhoneNumber' => $phoneNumber, // This might need to be edited from 27xx to 0xx
                'Amount' => ((float) $amount) / 100, // Convert amount to a cents value
            ]
        ];

        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');
        Log::info("BlueLabel-Api: GetTopup Request...", ['amount' => $amount, 'msisdn' => $phoneNumber, 'network' => $network, 'internalReference' => $reference, 'payload' => $arrayRequest, 'xml-payload' => $xmlRequest]);
        $response = $this->sendXmlRequest($xmlRequest, $this->socket);
        Log::info("BlueLabel-Api: GetTopup Response", [
            'transactionReference' => (string) $response->data->TransRef,
            'networkName' => (string) $response->data->SupplierName,
            'msisdn' => (string) $response->data->PhoneNumber,
            'amount' => (string) $response->data->Amount,
            'internalReference' => (string) $response->data->Reference,
            'externalReference' => (string) $response->data->Ref,
            'Date' => (string) $response->data->Date,
        ]);

        return $response;
    }

    public function bundleTopup(string $productCode, string $phoneNumber, string $network, string $reference = null )
    {
        if(!$reference){
            $reference = Str::random();
        }

        $arrayRequest = [
            'SessionId' => $this->authenticate($network),
            'EventType' => 'DoBundleTopup',
            'event' => [
                'Reference' => $reference,
                'PhoneNumber' => $phoneNumber, // This might need to be edited from 27xx to 0xx
                'ProductCode' => $productCode,
            ]
        ];

        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');
        Log::info("BlueLabel-Api: DoBundleTopup Request...", ['productCode' => $productCode, 'msisdn' => $phoneNumber, 'network' => $network, 'internalReference' => $reference, 'payload' => $arrayRequest, 'xml-payload' => $xmlRequest]);
        $response = $this->sendXmlRequest($xmlRequest, $this->socket);
        Log::info("BlueLabel-Api: DoBundleTopup Response", [
            'transactionReference' => (string)$response->data->TransRef,
            'networkName' => $network,
            'supplierName' => (string)$response->data->SupplierName,
            'msisdn' => (string)$response->data->PhoneNumber,
            'amount' => (string)$response->data->Amount,
            'productCode' => $productCode,
            'internalReference' => (string)$response->data->Reference,
            'externalReference' => (string)$response->data->Ref,
            'transactionSequence' => (string)$response->data->TransSeq,
            'callCenter' => (string)$response->data->CallCenter,
            'Date' => (string)$response->data->Date,
        ]);
        return $response;
    }

    public function getNetworkBundles($network): SimpleXMLElement
    {
        $arrayRequest = [
            'SessionId' => $this->getSessionId($network),
            'EventType' => 'GetProductList',
        ];

        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');
        Log::info('BlueLabel-Api: getNetworkBundle Request...');
        return $this->sendXmlRequest($xmlRequest, $this->socket);
    }

    public function reprint($transferReference = null, $originalReference = null, $phoneNumber = null): SimpleXMLElement
    {
        $arrayRequest = [
            'SessionId' => $this->getSessionId(),
            'EventType' => 'Reprint',
            'event' => [
            ]
        ];
        if($transferReference){
            $arrayRequest['event']['TransRef'] = $transferReference;
        }
        if($originalReference){
            $arrayRequest['event']['OrigReference'] = $originalReference;
        }
        if($phoneNumber){
            $arrayRequest['event']['PhoneNumber'] = $phoneNumber;
        }
        if(!$transferReference && !$originalReference && !$phoneNumber)
        {
            throw new ValidationException("BlueLabel-Api: One of the following are required for Reprints: TransferReference, OriginalReference, PhoneNumber");
        }
        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');

        Log::info('BlueLabel-Api: getNetworkBundle Request...');
        return $this->sendXmlRequest($xmlRequest, $this->socket);

    }

    public function getAccountInfo(): SimpleXMLElement
    {
        $arrayRequest = [
            'EventType' => 'Authentication',
            'event' => [
                'UserPin' => config('bluelabel.aeon.user_pin'),
                'DeviceId' => config('bluelabel.aeon.device_id'),
                'DeviceSer' => config('bluelabel.aeon.device_serial'),
                'TransType' => 'AccountInfo',
            ]
        ];
        $xmlRequest = ArrayToXml::convert($arrayRequest, 'request');

        Log::info('BlueLabel-Api: getNetworkBundle Request...');
        return $this->sendXmlRequest($xmlRequest);
    }

    protected function getAuthenticationRequestData(?string $network) : array
    {
        $data = [
            'EventType' => 'Authentication',
            'event' => [
                'UserPin' => config('bluelabel.aeon.user_pin'),
                'DeviceId' => config('bluelabel.aeon.device_id'),
                'DeviceSer' => config('bluelabel.aeon.device_serial'),
                'TransType' => $network ?? 'Vodacom',
            ]
        ];
        return $data;
    }

    protected function sendXmlRequest(string $xml, $socket = null,) : SimpleXMLElement
    {
        if(!$socket){
            $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
            if ($socket === false) {
                Log::error('BlueLabel-Api: Socket Failure', ['xml' => $xml,]);
                throw new SocketException("BlueLabel-API: Creating Socket failed: " . socket_strerror(socket_last_error()));
            }
            $result = socket_connect($socket, $this->hostname, $this->port);
            if ($result === false) {
                Log::error('BlueLabel-Api: Socket Failure', ['xml' => $xml,]);
                throw new SocketException("BlueLabel-API: Connecting Socket failed: " . socket_strerror(socket_last_error()));
            }
            $this->socket = $socket;
        }


        $xml = explode("\n", $xml, 2)[1]; // Apparently Aeon doesn't like the xml version specified :facepalm:

        $bytesToSend = strlen($xml);
        $bytesSent = socket_write($socket, $xml, $bytesToSend);
        if ($bytesSent === false) {
            Log::error('BlueLabel-Api: Socket Failure', ['xml' => $xml,]);
            throw new SocketException("BlueLabel-API: Writing to Socket failed: " . socket_strerror(socket_last_error()));
        }
        if ($bytesSent !== $bytesToSend) {
            Log::error('BlueLabel-Api: Stream Data mismatch', ['xml' => $xml,]);
            throw new SocketException("BlueLabel-API: Bytes sent mismatch. Bytes sent: " . $bytesSent. " Expected Bytes sent: " . $bytesToSend);
        }

        $write = NULL;
        $except = NULL;
        $sockets = [$socket];
        $stringResponse = '';
        while($sockets_changed = socket_select($sockets, $write, $except, $this->timeout)){
            $stringResponse .= socket_read($socket,10000, PHP_NORMAL_READ);
        }

        if($stringResponse === false || $stringResponse === "")
        {
            Log::error('BlueLabel-Api: Request Failure', ['xml' => $xml, 'response_str' => $stringResponse, 'socket_error' => socket_strerror(socket_last_error())]);
            throw new SocketException("BlueLabel-API: No valid response");
        }

        $xmlResponse = new SimpleXMLElement($stringResponse);
        if(isset($xmlResponse->event) && isset($xmlResponse->event->EventCode) && (int) $xmlResponse->event->EventCode == 1)
        {
            if(isset($xmlResponse->data?->ErrorCode))
            {
                $errorCode = $xmlResponse->data?->ErrorCode;
                switch ($errorCode)
                {
                    case static::ERROR_CODE_ACCOUNT_TYPE_NOT_VALID:
                        Log::error('BlueLabel-Api: Request Failure - AccountType not valid -POSTPAID', ['xml' => $xml]);
                        throw new ValidationException("$errorCode | AccountType not valid - POSTPAID", static::ERROR_CODE_ACCOUNT_TYPE_NOT_VALID);
                    case static::ERROR_CODE_MSISDN_VALIDATION_FAILED:
                        Log::error('BlueLabel-Api: Request Failure - MSISDN Validation Failed', ['xml' => $xml]);
                        throw new ValidationException("$errorCode | MSISDN Validation Failed", static::ERROR_CODE_MSISDN_VALIDATION_FAILED);
                    case static::ERROR_CODE_SUBSCRIBER_BARRED:
                        Log::error('BlueLabel-Api: Request Failure - Subscriber Barred', ['xml' => $xml]);
                        throw new ValidationException("$errorCode | Subscriber Barred", static::ERROR_CODE_SUBSCRIBER_BARRED);
                    default:
                        Log::error('BlueLabel-Api: Request Failure - Default Error', ['xml' => $xml, 'response_str' => $stringResponse]);
                        throw new ValidationException("$errorCode | BlueLabel-API: BlueLabel Error: " . $stringResponse, $xmlResponse->data?->ErrorCode);
                }
            }
            Log::error('BlueLabel-Api: Request Failure - Unknown Error', ['xml' => $xml, 'response_str' => $stringResponse]);
            throw new ValidationException("BlueLabel-API: BlueLabel Error: " . $stringResponse);
        }

        return $xmlResponse;
    }

    protected function getFromCache(string $key, array $tags = ['bluelabel'])
    {
        if (Cache::tags($tags)->has($key)) {
            Log::debug("BlueLabel-API: Retrieving collection from cache", ['tags' => $tags, 'key' => $key]);
            return Cache::tags($tags)->get($key);
        }
        return false;
    }

    protected function storeInCache(string $key, $data, $seconds, array $tags = ['bluelabel']): bool
    {
        Log::debug("BlueLabel-API: Saving to cache", ['tags' => $tags, 'key' => $key]);
        return Cache::tags($tags)->put($key, $data, $seconds);
    }
}
